const softwares_uri = {
    'Jitsi Meet': 'https://jitsi.org/',
    'CodiMD': 'https://demo.codimd.org/',
    'Etherpad': 'https://etherpad.org/',
    'Ethercalc': 'https://ethercalc.org/',
    'Framadate': 'https://framagit.org/framasoft/framadate',
    'Lufi': 'https://framagit.org/luc/lufi',
    'Lutim': 'https://github.com/ldidry/lutim',
    'LSTU': 'https://github.com/ldidry/lstu',
    'Scrumblr': 'https://github.com/aliasaria/scrumblr',
};
export default softwares_uri;
