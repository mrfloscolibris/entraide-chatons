const categories = {
  Visioconférence: {
    title: "service.visio_conf.title",
    placeholder: "service.visio_conf.placeholder",
    icon: "Jitsi.svg",
    description: "service.visio_conf.description",
    wiki: "https://wiki.chatons.org/doku.php/la_visio-conference_avec_jitsi",
    direct: true,
  },
  "Rédaction collaborative": {
    title: "service.pad.title",
    placeholder: "service.pad.placeholder",
    icon: "Etherpad.svg",
    description: "service.pad.description",
    wiki:
      "https://wiki.chatons.org/doku.php/la_redaction_collaborative_avec_etherpad",
    direct: true,
  },
  "Aide à la prise de rendez-vous": {
    title: "service.date.title",
    placeholder: "service.date.placeholder",
    icon: "Framadate.svg",
    description: "service.date.description",
    wiki:
      "https://wiki.chatons.org/doku.php/le_sondage_de_dates_avec_framadate",
    direct: false,
  },
  "Partage temporaire de fichiers": {
    title: "service.file_sharing.title",
    placeholder: "service.file_sharing.placeholder",
    icon: "Lufi.svg",
    description: "service.file_sharing.description",
    wiki: "https://wiki.chatons.org/doku.php/partage-temporaire-fichiers",
    direct: false,
  },
  "Tableau blanc collaboratif": {
    title: "service.postit.title",
    placeholder: "service.postit.placeholder",
    icon: "Scrumblr.svg",
    description: "service.postit.description",
    wiki: "",
    direct: true,
  },
  "Raccourcisseur d'URL": {
    title: "service.url_shortener.title",
    placeholder: "service.url_shortener.placeholder",
    icon: "Lstu.svg",
    description: "service.url_shortener.description",
    wiki: "https://wiki.chatons.org/doku.php/raccourcir_ses_liens_avec_lstu",
    direct: false,
  },
  "Stockage/partage d'images": {
    title: "service.picture_sharing.title",
    placeholder: "service.picture_sharing.placeholder",
    icon: "Lutim.svg",
    description: "service.picture_sharing.description",
    wiki:
      "https://wiki.chatons.org/doku.php/le_partage_temporaire_d_images_avec_lutim",
    direct: false,
  },
  "Tableur collaboratif": {
    title: "service.calc.title",
    placeholder: "service.calc.placeholder",
    icon: "chaton-surpris.png",
    description: "service.calc.description",
    wiki: "",
    direct: true,
  },
}
export default categories
