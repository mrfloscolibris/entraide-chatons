import {Button, Card, Col, Form, FormControl, InputGroup, Media, OverlayTrigger, Row, Tooltip} from "react-bootstrap";
import React, {useState} from "react";
import {withPrefix} from "gatsby-link";
import categories from "../data/services"
import softwares_uri from "../data/softwares"
import {useIntl} from "gatsby-plugin-intl";

/* Random alphanumeric name with 10 chars */
const name = () => [...Array(10)]
    .map(() => Math.random().toString(36)[3])
    .join("")
    .replace(/(.|$)/g, c =>
        c[!Math.round(Math.random()) ? "toString" : "toLowerCase"]()
    )

const buildEndpoint = chaton => {
    const hostname = new URL(chaton.endpoint).hostname
    if (chaton.Logiciels === "Etherpad") {
        if (chaton.Nom_du_chaton === "Framasoft") {
            return "https://bimestriel.framapad.org/p/"
        }
        return `https://${hostname}/p/`
    }
    return `https://${hostname}/`
}

export const ServiceCard = ({ serviceKey: key, instanceForService, changeInstanceForService, services }) => {
    const initialName = name()
    const [instanceName, setInstanceName] = useState(initialName)

    const regenerateName = () => {
        if (instanceName === '') {
            setInstanceName(name())
        }
    }

    const createInstance = chaton => {
        window.location = `${buildEndpoint(chaton)}${instanceName}`
    }
    const intl = useIntl()
console.log(key, instanceForService[key], instanceForService[key].software)
    return (<Card>
        <Card.Body>
            <Media>
                <img
                    width={90}
                    height={90}
                    className="mr-3"
                    src={withPrefix(`/pictures/${categories[key]['icon']}`)}
                    alt=""
                />
                <Media.Body>
                    <Card.Title>
                        {intl.formatMessage({ id: categories[key]['title'] })}
                    </Card.Title>
                    <Card.Text>
                        {intl.formatMessage({ id: categories[key]['description'] })}
                    </Card.Text>
                </Media.Body>
            </Media>
            <hr />
            <Row className="justify-content-between" style={{marginBottom: '1rem'}}>
                <Col>
                    <Card.Text className="provided-by">
                        {intl.formatMessage({ id: 'service.instance' })} <a href={softwares_uri[instanceForService[key].software]}>{instanceForService[key].software}</a>
                        {services[key].length > 1 && (
                            <OverlayTrigger
                                key="trigger{key}"
                                placement="top"
                                overlay={
                                    <Tooltip>
                                        {intl.formatMessage({ id: 'service.changeInstance' })}
                                    </Tooltip>
                                }
                            >
                                <button onClick={() => changeInstanceForService(key)} type={"button"} className="btn refresh">
                                <img
                                        src="data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='%23000' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-refresh-ccw'%3e%3cpolyline points='1 4 1 10 7 10'%3e%3c/polyline%3e%3cpolyline points='23 20 23 14 17 14'%3e%3c/polyline%3e%3cpath d='M20.49 9A9 9 0 0 0 5.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 0 1 3.51 15'%3e%3c/path%3e%3c/svg%3e"
                                        alt="refresh" />
                                </button>
                            </OverlayTrigger>
                        )}
                        <br />
                        {intl.formatMessage({ id: 'service.providedBy' })} <a href={instanceForService[key].chaton_website}>{instanceForService[key].chaton}</a>
                    </Card.Text>
                </Col>
            </Row>
            {categories[key].direct ? (
                <Form onSubmit={(e) => { e.preventDefault(); createInstance(instanceForService[key]); }}>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder={intl.formatMessage({ id: categories[key]['placeholder'] })}
                            aria-label="name-for-url"
                            aria-describedby="name-for-url"
                            onChange={(e) => setInstanceName(e.target.value)}
                            value={instanceName}
                            onBlur={() => regenerateName()}
                        />
                        <InputGroup.Append>
                            <Button variant="primary" type={'submit'}>{intl.formatMessage({ id: 'service.create' })}</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form>
            ) : (
                <a href={instanceForService[key]['endpoint']} className="btn btn-primary"
                   role="button">{intl.formatMessage({id: categories[key]['placeholder']})}</a>
            )
            }
            <a href={categories[key]['wiki']} className="btn btn-outline-secondary"
               role="button">{intl.formatMessage({id: 'service.other_instances'})}</a>
        </Card.Body>
    </Card>)
}
